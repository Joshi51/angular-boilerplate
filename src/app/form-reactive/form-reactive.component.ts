import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';

@Component({
  selector: 'app-form-reactive',
  templateUrl: './form-reactive.component.html',
  styleUrls: ['./form-reactive.component.css']
})
export class FormReactiveComponent implements OnInit {
  loginForm: FormGroup;
  uname: string;
  pass: string;
  opt: string;
  email: string;
  rank: number;
  gender: string;
  profilepic: string;
  hideForm: string;
  constructor(private frmbuilder: FormBuilder) {
    this.loginForm = frmbuilder.group({
      uname: new FormControl(this.uname, [
        Validators.required,
        Validators.minLength(4),
        Validators.pattern(`.+\\@.+\\..+`)
      ]),
      pass: new FormControl(this.pass, [
        Validators.required,
        Validators.minLength(4)
      ]),
      opt: new FormControl(this.opt, [
        Validators.required,
      ])
    });
  }
  ngOnInit() {
    this.hideForm = localStorage.getItem('login');
  }

  PostData(loginForm: NgForm) {
    const uname = loginForm.controls['uname'].value,
          pass = loginForm.controls['pass'].value,
          opt = loginForm.controls['opt'].value,
          data = {username: uname,
                  password: pass,
                  option: opt
            };
    fetch('http://localhost:3000/login', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => {
        if (response.failed) {
            console.log('failed');
        } else {

        }
        // localStorage.setItem('login', 'true');
        localStorage.setItem('username', response.firstname + ' ' + response.lastname);
        localStorage.setItem('rank', response.rank);
        localStorage.setItem('gender', response.gender);
        localStorage.setItem('profilepic', response.profilepic);
        localStorage.setItem('email', response.emailid);
        this.uname = response.firstname + ' ' + response.lastname;
        this.email = response.emailid;
        this.rank = response.rank;
        this.gender = response.gender;
        this.profilepic = response.profilepic;
        // this.hideForm = 'true';
      });
  }
}
