import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FormReactiveComponent} from './form-reactive/form-reactive.component';

const routes: Routes = [
  { path: 'form', component: FormReactiveComponent},
  {path: '', redirectTo: '/', pathMatch: 'full'}
];
@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ],
})

export class AppRoutingModule { }
